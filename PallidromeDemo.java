package bcas.str.pali;

public class PallidromeDemo {

	public void pallidromeDemo(String input) {
		checkPallidrome(input);
	}
	
	public String reverseWord(String word) {
		String revString = "";
		
		
		for (int i = word.length() - 1; i>=0; i--) {
			revString = revString + word.charAt(i);
		}
		return revString;
		
	}
	public boolean checkPallidrome(String input) {
		boolean isPallidrome = false;
		String revInput = reverseWord(input);
		if(input.equals(revInput))
			isPallidrome = true;
		
		displayPallidrome(input, isPallidrome);
		return isPallidrome;
	}

	private boolean checkpallidromeByChar(String input) {
		boolean isPallidrome = true;
		String revWord = reverseWord(input);
		for(int i = 0; i<input.length() - 1; i++) {
			char inputChar = input.charAt(i);
			char revInputChar = revWord.charAt(i);
			if (inputChar!=revInputChar) {
				isPallidrome = false;
			}	
			break;
		}
	return isPallidrome;
	}
	
			private void displayPallidrome(String word, boolean status) {
				if(status) {
					System.out.println(word + "Is a Pallidrome");
				}
				else {
					System.out.println(word + "Is not a Pallidrome");
				}
			}
		}
	
	
